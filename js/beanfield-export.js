//Beanfield Algorithm by TryCatch implement by JIW CS_CMU29

var sensors;
var sorted_sensors = [];
var width_dam = 1200, height_dam = 500;
var frame_cache = [];
var hex_cache = [];
var total_record;
var max_sea_water = 1000;
var min_sea_water = 0;


var indicatePa1 = "0.50";
var indicatePa2 = "-";
var indicatePa3 = "-";
var indicatePa4 = "-";
var indicatePa5 = "-";
var indicatePa6 = "-";

var debug = false;

//Coordinate Min Max for display in simulation 
var coor_sea_min = [];
var coor_sea_max = [];

function drawing(waterLevel, installSensor) {

    //Draw Dam Template
    $('canvas.damview').drawImage({
        source: $(".dam_template").text(),
        width: width_dam,
        height: height_dam,
        x: 600,
        y: 250
    });

    waterLevelRender(waterLevel);

    if (installSensor !== false) {
        $.each(sensors, function (i, v) {
            if (v.extend == 0) {
                $('canvas.damview').drawArc({
                    fillStyle: "black",
                    x: v.coor[0], y: v.coor[1],
                    radius: 2
                }).drawText({
                    fillStyle: '#25a',
                    strokeWidth: 1,
                    rotate: -25,
                    x: v.coor[0] - 23, y: v.coor[1] + 10,
                    fontSize: 10,
                    fontFamily: 'Verdana, sans-serif',
                    text: v.value.toFixed(4)
                });
            } else if (debug) {
                $('canvas.damview').drawArc({
                    fillStyle: "#ccc",
                    x: v.coor[0], y: v.coor[1],
                    radius: 1
                });

            }
        });
    }
}

function findKPA(KPA, hex_color) {
    var resultPoint = [];

    for (var sensor in sorted_sensors) {
        var num = parseInt(sensor);
        if (KPA <= sorted_sensors[sensor][2]) {
            for (var i = (num + 1); i < sorted_sensors.length; i++) {
                if (KPA >= sorted_sensors[i][2] && sorted_sensors[i][2] > 0) {
                    var line = Math.sqrt(Math.pow((sorted_sensors[sensor][1][1] - sorted_sensors[i][1][1]), 2) + Math.pow((sorted_sensors[sensor][1][0] - sorted_sensors[i][1][0]), 2));
                    var dif_kpa = Math.abs(sorted_sensors[sensor][2] - sorted_sensors[i][2]);
                    var weigth_plot = (sorted_sensors[sensor][2] - KPA) * (line / dif_kpa);
                    var dY = (sorted_sensors[sensor][1][1] - sorted_sensors[i][1][1]);
                    var dX = (sorted_sensors[sensor][1][0] - sorted_sensors[i][1][0]);
                    var xCal = (Math.abs(dX) / line) * weigth_plot;
                    var yCal = (Math.abs(dY) / line) * weigth_plot;
                    var resX = (dX > 0) ? sorted_sensors[sensor][1][0] - xCal : sorted_sensors[sensor][1][0] + xCal;
                    var resY = (dY > 0) ? sorted_sensors[sensor][1][1] - yCal : sorted_sensors[sensor][1][1] + yCal;
                    resultPoint.push([resX, resY]);
                }
            }
        } else {
            break;
        }
    }

    //Plot Value
    $.each(resultPoint, function (i, v) {
        $('canvas.damview').drawArc({
            fillStyle: hex_color,
            x: v[0], y: v[1],
            radius: 12,
            opacity: 0.4
        });
    });

}

function clearCanvas(waterLevel) {
    $('canvas.damview').clearCanvas();
}

function decimalToHexString(number) {
    if (number < 0) {
        number = 0xFFFFFFFF + number + 1;
    }
    if (!!(number % 1)) {
        number = number;

    }
    return "#" + number.toString(16).toUpperCase();
}

function waterLevelRender(sea_water_level) {

    if (sea_water_level <= min_sea_water) {
        return coor_sea_min;
    }

    //- find diff
    var diff_coor_y = coor_sea_min[1] - coor_sea_max[1];
    var diff_coor_x = coor_sea_max[0] - coor_sea_min[0];

    //- convert real sea to ratio meter as px
    var sea_lv_diff = max_sea_water - min_sea_water;
    var ratio = diff_coor_y / sea_lv_diff;

    var water_coor_y = diff_coor_y - ((sea_water_level - min_sea_water) * ratio);
    var water_coor_x = ((diff_coor_y - water_coor_y) * diff_coor_x) / diff_coor_y;

    //- Calculate for Waterlevel indicator line
    water_coor_y += coor_sea_max[1];
    water_coor_x += coor_sea_min[0];

    $('canvas.damview').drawText({
        fillStyle: '#9cf',
        strokeStyle: '#25a',
        strokeWidth: 1,
        x: water_coor_x - 50,
        y: water_coor_y - 10,
        fontSize: 10,
        fontFamily: 'Verdana, sans-serif',
        text: "Water level: " + (sea_water_level).toFixed(4)
    }).drawLine({
        strokeStyle: '#25a',
        strokeWidth: 2,
        x1: water_coor_x,
        x2: coor_sea_min[0],
        y1: water_coor_y,
        y2: water_coor_y
    });
}

function drawCache(base_64_data) {
    $('canvas.damview').clearCanvas().drawImage({
        source: base_64_data,
        width: width_dam,
        height: height_dam,
        x: 600,
        y: 250
    });
}

function drawIndicator() {
    //Drawing indicator

    var startXConst = 790;
    var startYConst = 0;
    var ctx = $('canvas.damview').get(0).getContext("2d");
    // Create gradient
    var grd = ctx.createLinearGradient(800, 0, 1180, 0);
    grd.addColorStop(0, "#66ffff");
    grd.addColorStop(0.2, "#66ff99");
    grd.addColorStop(0.4, "#ffcc66");
    grd.addColorStop(0.6, "#ff9966");
    grd.addColorStop(0.8, "#ff6666");
    grd.addColorStop(1, "#ff0000");
    // Fill with gradient
    ctx.beginPath();
    ctx.fillStyle = grd;
    ctx.fillRect(800, 70, 380, 20);


    var transposingConstX_Pa0 = 0 + startXConst;
    var transposingConstY_Pa0 = 90 + startYConst;

    ctx.beginPath();
    ctx.moveTo(10 + transposingConstX_Pa0, 0 + transposingConstY_Pa0);
    ctx.lineTo(5 + transposingConstX_Pa0, 10 + transposingConstY_Pa0);
    ctx.lineTo(15 + transposingConstX_Pa0, 10 + transposingConstY_Pa0);
    ctx.fillStyle = "black";
    ctx.fill();

    ctx.font = "10px Sans-serif";
    ctx.textAlign = "center";
    ctx.fillText(indicatePa1, 10 + transposingConstX_Pa0, 20 + transposingConstY_Pa0);

    // section controlling Point PA1
    var transposingConstX_Pa1 = 76 + startXConst;
    var transposingConstY_Pa1 = 90 + startYConst;

    ctx.beginPath();
    ctx.moveTo(10 + transposingConstX_Pa1, 0 + transposingConstY_Pa1);
    ctx.lineTo(5 + transposingConstX_Pa1, 10 + transposingConstY_Pa1);
    ctx.lineTo(15 + transposingConstX_Pa1, 10 + transposingConstY_Pa1);
    ctx.fillStyle = "black";
    ctx.fill();

    ctx.font = "10px Sans-serif";
    ctx.textAlign = "center";
    ctx.fillText(indicatePa2, 10 + transposingConstX_Pa1, 20 + transposingConstY_Pa1);

    // section controlling Point PA2
    var transposingConstX_Pa2 = 152 + startXConst;
    var transposingConstY_Pa2 = 90 + startYConst;

    ctx.beginPath();
    ctx.moveTo(10 + transposingConstX_Pa2, 0 + transposingConstY_Pa2);
    ctx.lineTo(5 + transposingConstX_Pa2, 10 + transposingConstY_Pa2);
    ctx.lineTo(15 + transposingConstX_Pa2, 10 + transposingConstY_Pa2);
    ctx.fillStyle = "black";
    ctx.fill();

    ctx.font = "10px Sans-serif";
    ctx.textAlign = "center";
    ctx.fillText(indicatePa3, 10 + transposingConstX_Pa2, 20 + transposingConstY_Pa2);
    // section controlling Point PA3
    var transposingConstX_Pa3 = 228 + startXConst;
    var transposingConstY_Pa3 = 90 + startYConst;

    ctx.beginPath();
    ctx.moveTo(10 + transposingConstX_Pa3, 0 + transposingConstY_Pa3);
    ctx.lineTo(5 + transposingConstX_Pa3, 10 + transposingConstY_Pa3);
    ctx.lineTo(15 + transposingConstX_Pa3, 10 + transposingConstY_Pa3);
    ctx.fillStyle = "black";
    ctx.fill();
    ctx.font = "10px Sans-serif";
    ctx.textAlign = "center";
    ctx.fillText(indicatePa4, 10 + transposingConstX_Pa3, 20 + transposingConstY_Pa3);
    // section controlling Point PA4
    var transposingConstX_Pa4 = 304 + startXConst;
    var transposingConstY_Pa4 = 90 + startYConst;

    ctx.beginPath();
    ctx.moveTo(10 + transposingConstX_Pa4, 0 + transposingConstY_Pa4);
    ctx.lineTo(5 + transposingConstX_Pa4, 10 + transposingConstY_Pa4);
    ctx.lineTo(15 + transposingConstX_Pa4, 10 + transposingConstY_Pa4);
    ctx.fillStyle = "black";
    ctx.fill();

    ctx.font = "10px Sans-serif";
    ctx.textAlign = "center";
    ctx.fillText(indicatePa5, 10 + transposingConstX_Pa4, 20 + transposingConstY_Pa4);
    // section controlling Point PA5
    var transposingConstX_Pa5 = 380 + startXConst;
    var transposingConstY_Pa5 = 90 + startYConst;

    ctx.beginPath();
    ctx.moveTo(10 + transposingConstX_Pa5, 0 + transposingConstY_Pa5);
    ctx.lineTo(5 + transposingConstX_Pa5, 10 + transposingConstY_Pa5);
    ctx.lineTo(15 + transposingConstX_Pa5, 10 + transposingConstY_Pa5);
    ctx.fillStyle = "black";
    ctx.fill();

    ctx.font = "10px Sans-serif";
    ctx.textAlign = "center";
    ctx.fillText(indicatePa6, 10 + transposingConstX_Pa5, 20 + transposingConstY_Pa5);
    return 1;
}


function MainControll(resp) {

//    if (resp) {
//        total_record = resp.length;
        var index = 0;
        //Color real not working for simulation
        //var finite_shade = ['#ff0000','#ff5500','#aaff00','#00ff00','#00ffaa','#0055ff','#0000ff'];
        frame_cache = [];
        hex_cache = [];

//        setInterval(function () {

            /**
             * Detect frame cache
             */
//            if (!frame_cache[index]) {
                /**
                 * Render Simulation 1 frame and cache it
                 */
                sorted_sensors = [];
                var data = resp[index];
                sensors = data.data;

                frame_cache[index] = {};

                var waterLevel = data.head;
                frame_cache[index]['time'] = data.time;
                $("#time_step").text(data.time);

                clearCanvas(waterLevel);

                var min = 0;
                for (var sensor in sensors) {
                    sorted_sensors.push([sensor, sensors[sensor].coor, sensors[sensor].value]);
                    if (min === 0) {
                        min = sensors[sensor].value;
                    }

                    if (min > sensors[sensor].value && sensors[sensor].value > 0) {
                        min = sensors[sensor].value;
                    }
                }

                sorted_sensors.sort(function (a, b) {
                    return b[2] - a[2];
                });

                var max = Math.ceil(sorted_sensors[0][2]) + 2;
                var realMax = sorted_sensors[0][2];
                min = Math.floor(min);
                if (min === 0) {
                    min = 0;
                }

                var T = 1;

                var range = Math.ceil(max / 5);

                //find value to plot
                for (var i = min; i <= max; i += 0.5) {

                    var plot = true;
                    var hex_color = "gray";

                    //Range main shade
                    if ((i > 0) && i <= (range * 1)) {
                        //shade 1
                        hex_color = decimalToHexString((parseInt(0x66ffff)) - ((((parseInt(0x66ffff) - parseInt(0x66ff99)) / 9)) * (Math.floor(((9 / range) * (i))))));
                    } else if (i > (range * 1) && i <= (range * 2)) {
                        //shade 2
                        hex_color = decimalToHexString((parseInt(0x66ff99)) + ((((parseInt(0xccff66) - parseInt(0x66ff99)) / 9)) * (Math.floor(((9 / range) * (i - ((range * 1))))) * T)));
                    } else if (i > (range * 2) && i <= (range * 3)) {
                        //shade 3
                        hex_color = decimalToHexString((parseInt(0xffcc66)) - ((((parseInt(0xffcc66) - parseInt(0xff9966)) / 9)) * (Math.floor(((9 / range) * (i - ((range * 2))))) * T)));
                    } else if ((i > (range * 3) && i <= (range * 4))) {
                        //shade 4
                        hex_color = decimalToHexString((parseInt(0xff9966)) - ((((parseInt(0xff9966) - parseInt(0xff6666)) / 9)) * (Math.floor(((9 / range) * (i - ((range * 3))))) * T)));
                    } else if ((i > (range * 4) && i <= max)) {
                        //shade 5
                        hex_color = decimalToHexString((parseInt(0xff6666)) - ((((parseInt(0xff6666) - parseInt(0xff0000)) / 9)) * (Math.floor(((9 / range) * (i - ((range * 4))))) * T)));
                    } else {
                        console.log("Error", i);
                        plot = false;
                    }

                    if (plot) {
                        /**
                         * Render color on simulation
                         */
                        findKPA(i, hex_color);
                    }
                }
                /**
                 * setting indicator value
                 */

                indicatePa2 = ((realMax * 1) / 5).toFixed(2);
                indicatePa3 = ((realMax * 2) / 5).toFixed(2);
                indicatePa4 = ((realMax * 3) / 5).toFixed(2);
                indicatePa5 = ((realMax * 4) / 5).toFixed(2);
                indicatePa6 = realMax.toFixed(2);


                /**
                 * Render Water Level
                 */


                drawing(waterLevel);
                while (!drawIndicator())
                    ;
                /**
                 * Cache the final result
                 */
                var cache_canvas = document.getElementById("myCanvas");
                frame_cache[index]['base64'] = cache_canvas.toDataURL();

//            } else {
//                /**
//                 * Frame cache is hit
//                 */
//                $("#time_step").text(frame_cache[index]['time']);
//                drawCache(frame_cache[index]['base64']);
//            }

//            index = (index + 1) % total_record;

//        }, 2500);
//    }
}