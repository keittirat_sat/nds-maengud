<?php
use GuzzleHttp\Client;

class HomeController extends BaseController
{

    private $pressure_repo;
    private $seismic_repo;
    private $profile;

    public function __construct()
    {
        $this->pressure_repo = App::make('PressureRepository');
        $this->seismic_repo  = App::make('SeismicRepository');
        $this->profile       = RegisterProfile::currentProfile();
        AssetMinify::instance()->build();
    }

    public function getIndex()
    {
        if (Config::get("{$this->profile}/nds-cloud.pressure.enable") == true) {
            $start_date = Input::get("start_date", date("Y-m-d") . " 00:00");
            $end_date   = Input::get("end_date", date("Y-m-d H:i"));

            $pressure_data = $this->pressure_repo->getDataByDate($start_date, $end_date);

            if (isset($pressure_data['result']) && count($pressure_data['result']['stream']) > 0) {
                $enable_extend_point = Config::get("{$this->profile}/nds-cloud.pressure.extend_point");

                $raws_json         = $this->pressure_repo->convertDataForDamview($pressure_data['result']['stream'], $unit              = "kpa", $enable_extend_point);
                $dam_template_path = public_path(Config::get("{$this->profile}/nds-cloud.pressure.dam_section"));
                $dam_template      = App::make("DamRepository")->base64_encode_image($dam_template_path, "png");
                $data              = [
                    "start_date"   => $start_date,
                    "end_date"     => $end_date,
                    "damview"      => $raws_json,
                    "dam_template" => $dam_template,
                    "error"        => 0,
                    "spec"         => array(
                        "water_level" => array(
                            "min" => Config::get("{$this->profile}/nds-cloud.pressure.min_sea_water"),
                            "max" => Config::get("{$this->profile}/nds-cloud.pressure.max_sea_water")
                        ),
                        "coordinate"  => Config::get("{$this->profile}/nds-cloud.pressure.simulation_template.water_level")
                    )
                ];
            } else {
                $data = [
                    "start_date"   => $start_date,
                    "end_date"     => $end_date,
                    "damview"      => 0,
                    "dam_template" => 0,
                    "error"        => 1,
                    "spec"         => false
                ];
            }

            $data['profile'] = $this->profile;
            $data['hash']    = AssetMinify::instance()->getHash();
            return View::make("page.index", $data);
        } else {
            return Redirect::action("HomeController@getSeismicEvent");
        }
    }

    public function getPressureGraph()
    {
        if (Config::get("{$this->profile}/nds-cloud.pressure.enable") == true) {
            $start_date = Input::get("start_date", date("Y-m-d") . " 00:00");
            $end_date   = Input::get("end_date", date("Y-m-d H:i"));

            $sensor = Input::get("sensor", 1);

            $pressure_data   = $this->pressure_repo->getDataByDate($start_date, $end_date);
            $pressure_data   = $this->pressure_repo->convertDataForGraph($pressure_data['result']['stream'], $sensor);
            $data            = [
                "group_date" => $pressure_data['timestamp'],
                "value"      => $pressure_data['pressure'],
                'sensor'     => $sensor,
                'field'      => "Piezometer - {$sensor}",
                "start_date" => $start_date,
                "end_date"   => $end_date
            ];
            $data['profile'] = $this->profile;
            return View::make("page.graph", $data);
        } else {
            return Redirect::action("HomeController@getSeismicEvent");
        }
    }

    public function getSeismicEvent()
    {
        if (Config::get("{$this->profile}/nds-cloud.seismic.enable") == true) {
            $records_per_page = 25;
            $page             = Input::get('page', 1);
            $data             = $this->seismic_repo->getEvent($page, $records_per_page);
            $current_status   = $this->seismic_repo->getStatus();
            if (!$data) {
                /**
                 * Cannot sync data
                 */
                $data = [
                    "data" => [],
                    "info" => [
                        "total_data" => 0
                    ],
                    "page" => [
                        "current_page" => 1,
                        "total_page"   => 1
                    ]
                ];
            } else {
                $data = $data["result"];
            }
            $data['records_per_page'] = $records_per_page;
            $data['profile']          = $this->profile;
            $data['status']           = $current_status;
            return View::make("page.event", $data);
        } else {
            return Redirect::action("HomeController@getPressureGraph");
        }
    }

    public function getDamsection($section)
    {
        
    }

    public function getSeismic($station = NULL, $time = NULL)
    {
        if (Config::get("{$this->profile}/nds-cloud.seismic.enable") == true) {
            $start_date  = Input::get("start_date", null);
            $get_station = Input::get("station", null);
            $format      = Input::get('format', 'web');
            $station     = is_null($get_station) ? $station : $get_station;
            if ($start_date == NULL) {
                $current = is_null($time) ? date("Y-m-d H:i:s") : $time;
            } else {
                $current = $start_date;
            }

            $resp = $this->seismic_repo->getSeismicData($station, $current);
            if ($format == 'json') {
                return Response::json($resp);
            }

            $current_status = $this->seismic_repo->getStatus();
            if (count($resp)) {
                $raws_data     = $resp["result"]["stream"];
//                $data          = $this->seismic_repo->convertDataToHiChart($resp["result"]["stream"]);
                $data["error"] = 0;
            } else {
                $raws_data = [];
                $data      = [
//                    "ch0"   => "[]",
//                    "ch1"   => "[]",
//                    "ch2"   => "[]",
                    "error" => 1
                ];
            }
            $data["station"]   = $station;
            $data["current"]   = $current;
            $data['status']    = $current_status;
            $data["resp"]      = $resp;
            $data["raws_data"] = $raws_data;
            $data['profile']   = $this->profile;

            // var_dump($data);
            // die();

            return View::make("page.seismic", $data);
        } else {
            return Redirect::action("HomeController@getPressureGraph");
        }
    }
}
