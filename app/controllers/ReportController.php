<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportController
 *
 * @author keitt
 */
class ReportController extends BaseController
{

    private $pressure_repo;
    private $seismic_repo;
    private $profile;

    public function __construct()
    {
        $this->pressure_repo = App::make('PressureRepository');
        $this->seismic_repo  = App::make('SeismicRepository');
        $this->profile       = RegisterProfile::currentProfile();
    }

    public function getPressure()
    {
        $date       = Input::get('date', date('Y-m-d'));
        $tempalate  = Input::get('template', 1);
        $start_date = $date . ' 00:00';
        $end_date   = $date . ' 23:59';

        $pressure_data = $this->pressure_repo->getDataByDate($start_date, $end_date);
        $raws_json     = $this->pressure_repo->convertDataForDamview($pressure_data['result']['stream'], "kpa", true);

        $dam_template      = false;
        if ($tempalate == true) {
            $dam_template_path = public_path(Config::get("{$this->profile}/nds-cloud.pressure.dam_section"));
            $dam_template      = App::make("DamRepository")->base64_encode_image($dam_template_path, "png");
        }

        $data = array(
            "code"            => 200,
            "template_base64" => $dam_template,
            "pressure_data"   => json_decode($raws_json)
        );

        return Response::json($data);
    }

    /**
     * Will Deprecate in nds-report
     * @return type
     */
    public function getSimulate()
    {
        $start_date = Input::get("start_date", date("Y-m-d") . " 00:00");
        $end_date   = Input::get("end_date", date("Y-m-d H:i"));

        $pressure_data     = $this->pressure_repo->getDataByDate($start_date, $end_date);
        $raws_json         = $this->pressure_repo->convertDataForDamview($pressure_data['result']['stream'], "kpa", true);
        $dam_template_path = public_path(Config::get("{$this->profile}/nds-cloud.pressure.dam_section"));
        $dam_template      = App::make("DamRepository")->base64_encode_image($dam_template_path, "png");

        $data = array(
            "code"            => 200,
            "template_base64" => $dam_template,
            "pressure_data"   => json_decode($raws_json)
        );

        return Response::json($data);
    }
}
