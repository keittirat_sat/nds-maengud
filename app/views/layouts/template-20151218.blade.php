<?php $profile   = RegisterProfile::currentProfile(); ?>
<?php $hash      = AssetMinify::instance()->getHash(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo isset($title) ? $title : "ระบบรายงานสภาพเขื่อน" ?> | <?php echo Config::get("{$profile}/nds-cloud.site_name"); ?></title>
        <?php echo HTML::style("css/app.min.css?v=" . $hash['app-css']); ?>
        <?php echo HTML::script("js/jquery-1.7.min.js"); ?>  
        <?php echo HTML::script("js/app.min.js?v=" . $hash['app-js']); ?>  
        <?php echo HTML::script("js/moment.min.js"); ?>  
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
    </head>
    <body>

        <div class="header_menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <img src="<?php echo asset("image/logo_white.png"); ?>" alt="" class="img-responsive" />
                        <div class="dropdown hidden-md hidden-lg" style="position: absolute; top: 0px; left: 10px;">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu_menu" data-toggle="dropdown">
                                <i class="glyphicon glyphicon glyphicon-align-justify"></i>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu_menu">
                                <?php $url_prop  = Config::get('nds-profile.' . $profile); ?>
                                <?php $url_prop  = array_flip($url_prop); ?>
                                <?php $env_index = $url_prop[Request::server("HTTP_HOST")]; ?>
                                <li role="presentation" class="dropdown-header">รายชื่ออ่างเก็บน้ำในระบบ</li>
                                <?php foreach (Config::get('nds-profile') as $menu_station => $url_list): ?>
                                    <li role="presentation" <?php echo $url_list[$env_index] == Request::server("HTTP_HOST") ? 'class="active"' : ''; ?>>
                                        <a role="menuitem" tabindex="-1" href="<?php echo 'http://' . $url_list[$env_index]; ?>"><?php echo Config::get($menu_station . '/nds-cloud.site_name'); ?></a>
                                    </li>
                                <?php endforeach; ?>
                                <li role="presentation" class="divider"></li>
                                <?php if (Config::get("{$profile}/nds-cloud.pressure.enable") == true): ?>
                                    <li class="element_menu"><a href="<?php echo action("HomeController@getIndex"); ?>">ระบบจำลองเหตุการณ์</a></li>
                                    <li role="presentation" class="divider"></li>
                                    <li class="element_menu"><a href="<?php echo action("HomeController@getPressureGraph"); ?>">แสดงผลกราฟค่าแรงดัน</a></li>
                                    <li role="presentation" class="divider"></li>
                                <?php endif; ?>

                                <!--                                <li role="presentation" class="dropdown-header">แสดงผลค่าแรงดันล่าสุดตามจุดติดตั้ง</li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getDamsection", ["section" => "km0180"]); ?>">กม. 0+180</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getDamsection", ["section" => "km0220"]); ?>">กม. 0+220</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getDamsection", ["section" => "km0280"]); ?>">กม. 0+280</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getDamsection", ["section" => "km0380"]); ?>">กม. 0+380</a></li>-->

                                <!--<li role="presentation" class="divider"></li>-->
                                <?php if (Config::get("{$profile}/nds-cloud.seismic.enable") == true): ?>
                                    <li role="presentation" class="dropdown-header">สรุปเหตุการณ์</li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getSeismicEvent"); ?>">ประมวลเหตุการณ์แรงสั่นสะเทือนที่ผ่านมา</a></li>

                                    <li role="presentation" class="dropdown-header">กราฟแสดงค่าการสั่นสะเทือน</li>
                                    <?php foreach (Config::get("{$profile}/nds-cloud.seismic.station") as $each_station => $each_label): ?>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getSeismic", ["station" => $each_station]); ?>"><?php echo $each_label; ?></a></li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row hidden-sm hidden-xs" style="padding-top: 45px;">
                    <div class="col-xs-12 quark_bold">
                        <ul class="nav nav-pills txt_right menu_blue">
                            <?php if (Config::get("{$profile}/nds-cloud.seismic.enable") == true): ?>
                                <li class="element_menu">
                                    <a href="#" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                        กราฟแสดงค่าการสั่นสะเทือน
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">                                
                                        <li role="presentation" class="dropdown-header">สรุปเหตุการณ์</li>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getSeismicEvent"); ?>">ประมวลเหตุการณ์แรงสั่นสะเทือนที่ผ่านมา</a></li>
                                        <li role="presentation" class="dropdown-header">กราฟแสดงค่าการสั่นสะเทือน</li>
                                        <?php foreach (Config::get("{$profile}/nds-cloud.seismic.station") as $each_station => $each_label): ?>
                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getSeismic", ["station" => $each_station]); ?>"><?php echo $each_label; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>
                            <!--                            <li class="element_menu" style="margin-left: 2px;">
                                                            <a href="#" class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown">
                                                                แสดงผลค่าแรงดันล่าสุดตามจุดติดตั้ง
                                                                <span class="caret"></span>
                                                            </a>
                                                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
                                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getDamsection", ["section" => "km0180"]); ?>">กม. 0+180</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getDamsection", ["section" => "km0220"]); ?>">กม. 0+220</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getDamsection", ["section" => "km0280"]); ?>">กม. 0+280</a></li>
                                                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo action("HomeController@getDamsection", ["section" => "km0380"]); ?>">กม. 0+380</a></li>
                                                            </ul>
                                                        </li>-->
                            <?php if (Config::get("{$profile}/nds-cloud.pressure.enable") == true): ?>
                                <li class="element_menu"><a href="<?php echo action("HomeController@getPressureGraph"); ?>">แสดงผลกราฟค่าแรงดัน</a></li>
                                <li class="element_menu"><a href="<?php echo action("HomeController@getIndex"); ?>">ระบบจำลองเหตุการณ์</a></li>
                            <?php endif; ?>
                            <li class="element_menu" style="margin-left: 2px;">
                                <a href="#" class="dropdown-toggle" type="button" id="dropdownMenu0" data-toggle="dropdown">
                                    รายชื่ออ่างเก็บน้ำในระบบ
                                    <span class="caret"></span>
                                </a>
                                <?php $url_prop  = Config::get('nds-profile.' . $profile); ?>
                                <?php $url_prop  = array_flip($url_prop); ?>
                                <?php $env_index = $url_prop[Request::server("HTTP_HOST")]; ?>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu0">
                                    <?php foreach (Config::get('nds-profile') as $menu_station => $url_list): ?>
                                        <li role="presentation" <?php echo $url_list[$env_index] == Request::server("HTTP_HOST") ? 'class="active"' : ''; ?>>
                                            <a role="menuitem" tabindex="-1" href="<?php echo 'http://' . $url_list[$env_index]; ?>"><?php echo Config::get($menu_station . '/nds-cloud.site_name'); ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class='container' style="padding-top: 20px;">


            @yield('content')            


            <div class='row'>
                <div class='col-md-12 light_blue clearfix'>
                    <div style='float: left;'><img src="<?php echo asset("image/footer_logo.png") ?>" alt="" /></div>
                    <div style='float: left; margin-left: 6px; font-size: 12px;'>
                        <p style='margin: 8px 0px 0px;'>(2014) by Regional Irrigation Office 1 of Royal Irrigation Department, Thailand</p>
                        <p style='margin: 0px;'>Brought to you by NDS | TryCatch&trade;</p>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-42013443-9', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>
