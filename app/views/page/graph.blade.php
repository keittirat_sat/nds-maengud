@extends('layouts.template-20151218')
@section('content')

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<div class="row">
    <div class="col-md-4">
        <h1 class='gray quark_bold margin_bottom_0'>โครงการ<?php echo Config::get("{$profile}/nds-cloud.site_name"); ?></h1>
        <p class="gray">ระบบรายงานสภาพแรงดันภายในเขื่อน</p>
        <div class="form-horizontal" role="form">
            <div class="form-group margin_bottom_0">
                <label for="forSensor" class="col-sm-4 control-label txt_left">เลือกหัวอ่าน</label>
                <div class="col-sm-8 trycatch">
                    <select id="sensor">
                        <?php for ($i = Config::get("{$profile}/nds-cloud.pressure.total.start"); $i <= Config::get("{$profile}/nds-cloud.pressure.total.end"); $i++): ?>
                            <option value="<?php echo $i; ?>" <?php echo $i == $sensor ? "selected":"";?>><?php echo "Piezometer NO. {$i}"; ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 txt_right">
        <div class="col-md-12 txt_right">
            <div style="margin-top: 12px;">
                <h5><b>เลือกช่วงเวลาที่ต้องการตรวจสอบ</b></h5>
                <form  class="form-horizontal" role="form">
                    <div class="form-group margin_bottom_0">
                        <!--<label for="forStartDate" class="col-sm-4 control-label">เวลาเริ่ม</label>-->
                        <div class="col-sm-12">
                            <label class="hidden-sm hidden-xs" for="forStartDate" style="margin-top: 0; margin-bottom: 0; padding: 7px 5px 0px 0px;">เวลาเริ่ม</label>
                            <span>
                                <input type="text" name="start_date" id="start_datetime24" data-format="YYYY-MM-DD HH:mm" data-template="DD / MM / YYYY     HH : mm" name="datetime" value="<?php echo $start_date ?>">
                            </span>
                        </div>
                    </div>
                    <div class="form-group margin_bottom_0">
                        <!--<label for="forEndDate" class="col-sm-4 control-label">เวลาสิ้นสุด</label>-->
                        <div class="col-sm-12">
                            <label class="hidden-sm hidden-xs" for="forEndDate" style="margin-top: 0; margin-bottom: 0; padding: 7px 5px 0px 0px;">เวลาสิ้นสุด</label>
                            <input type="text" id="end_datetime24" name="end_date" data-format="YYYY-MM-DD HH:mm" data-template="DD / MM / YYYY     HH : mm" name="datetime" value="<?php echo $end_date ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4 txt_right">
                            <button type="submit" class="btn btn-info btn-sm">Submit</button>
                            <!--<button type="button" class="btn btn-warning btn-sm" id="download_csv">Download CSV</button>-->
                            <button type="button" class="btn btn-danger btn-sm" id="reset_btn">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class='row margin_bottom_20'>
    <div class='col-md-12'>
        <div id='display_chart' style="width: 100%; min-height: 400px; margin: 0px auto;">

        </div>
    </div>
</div>

<script type='text/javascript'>
    $(function () {
        $('#start_datetime24').combodate();
        $('#end_datetime24').combodate();
        $('#sensor').change(function () {
            var start_date = $('#start_datetime24').val();
            var end_date = $('#end_datetime24').val();
            var sensor = $(this).val();
            var url = location.origin + location.pathname + "?sensor=" + sensor;
            url += "&start_date=" + start_date;
            url += "&end_date=" + end_date;

            location.href = url;
        });
        $('#display_chart').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Electronic Piezometre'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Source: NDS-Clound' :
                        'Pinch the chart to zoom in'
            },
            xAxis: {
                categories:<?php echo json_encode($group_date) ?>,
                labels: {
                    step: <?php echo (count($group_date) / 24) <= 0 ? 1 : ceil(count($group_date) / 24) ?>,
                    rotation: -45
                }
            },
            yAxis: {
                title: {
                    text: 'Pressure (KPA)'
                },
                plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
            },
            tooltip: {
                valueSuffix: 'KPA'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0,
            },
            series: [{
                    "name": "<?php echo $field ?>",
                    "data": <?php echo json_encode($value); ?>
                }],
            plotOptions: {
                line: {
                    dashStyle: 'ShortDot'
                }
            }
        });

        $('#reset_btn').click(function () {
            location.href = location.origin + location.pathname;
        });

        $('#download_csv').click(function () {
            var start_date = $('#start_datetime24').val();
            var end_date = $('#end_datetime24').val();
            var url = "?start_date=" + start_date;
            url += "&end_date=" + end_date;
            location.href = url;
        });

    });
</script>

@stop