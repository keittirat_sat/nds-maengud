@extends('layouts.template-20151218')
@section('content')


<div class="row">
    <div class="col-md-4">
        <h1 class='gray quark_bold margin_bottom_0'>โครงการ<?php echo Config::get("{$profile}/nds-cloud.site_name"); ?></h1>
        <p class="gray">ระบบรายงานสภาพแรงดันภายในเขื่อน</p>
    </div>
    <div class="col-md-8 txt_right">
        <div class="col-md-12 txt_right">
            <div style="margin-top: 12px;">
                <h5><b>เลือกช่วงเวลาที่ต้องการตรวจสอบ</b></h5>
                <form  class="form-horizontal" role="form">
                    <div class="form-group margin_bottom_0">
                        <!--<label for="forStartDate" class="col-sm-4 control-label">เวลาเริ่ม</label>-->
                        <div class="col-sm-12">
                            <label class="hidden-sm hidden-xs" for="forStartDate" style="margin-top: 0; margin-bottom: 0; padding: 7px 5px 0px 0px;">เวลาเริ่ม</label>
                            <span>
                                <input type="text" name="start_date" id="start_datetime24" data-format="YYYY-MM-DD HH:mm" data-template="DD MM YYYY HH : mm" name="datetime" value="<?php echo $start_date ?>">
                            </span>
                        </div>
                    </div>
                    <div class="form-group margin_bottom_0">
                        <!--<label for="forEndDate" class="col-sm-4 control-label">เวลาสิ้นสุด</label>-->
                        <div class="col-sm-12">
                            <label class="hidden-sm hidden-xs" for="forEndDate" style="margin-top: 0; margin-bottom: 0; padding: 7px 5px 0px 0px;">เวลาสิ้นสุด</label>
                            <input type="text" id="end_datetime24" name="end_date" data-format="YYYY-MM-DD HH:mm" data-template="DD MM YYYY HH : mm" name="datetime" value="<?php echo $end_date ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4 txt_right">
                            <button type="submit" class="btn btn-info btn-sm">Submit</button>
                            <!--<button type="button" class="btn btn-warning btn-sm" id="download_csv">Download CSV</button>-->
                            <button type="button" class="btn btn-danger btn-sm" id="reset_btn">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if ($error == 0): ?>
    <div class="row">
        <div class="col-md-12">    
            <div class="dam_sim_label">        
                <div class="timeline_step"><i class="glyphicon glyphicon-time"></i> จำลองสถานการณ์ <span id="time_step"></span></div>
            </div>
            <div class="sim_start_btn">
                <button class="btn btn-danger btn-lg" id="simulate_start"><i class="glyphicon glyphicon-play-circle"></i> เริ่มจำลองสถานการณ์</button>
            </div>

            <canvas id="myCanvas" class="damview" width="1200" height="500"></canvas>
        </div>
    </div>

    <div class="hidden data_damview">
        <?php echo $damview; ?>
    </div>

    <div class="dam_template hidden">    
        <?php echo $dam_template; ?>
    </div>

    <div class="dam_spec hidden">    
        <?php echo json_encode($spec); ?>
    </div>

    <style>
        .damview{
            width: 100%;
            background: #fff;
        } 

        .dam_sim_label{
            position: absolute;
            top: 20px;
            left: 20px;
        }

        .timeline_step{
            display: none;
        }

        .sim_start_btn{
            position: absolute;
            top: 0;
            bottom: 0;
            margin: auto;
            left: 0;
            right: 0;
            height: 47px;
            text-align: center;
        }
    </style>

    <script type='text/javascript'>
        $(function () {
            var spec = JSON.parse($('.dam_spec').text());
            
            min_sea_water = spec.water_level.min;
            max_sea_water = spec.water_level.max;
            coor_sea_min = spec.coordinate.min;
            coor_sea_max = spec.coordinate.max;

            $('#download_csv').click(function () {
                var start_date = $('#start_datetime24').val();
                var end_date = $('#end_datetime24').val();
                var url = "?start_date=" + start_date;
                url += "&end_date=" + end_date;
                location.href = url;
            });

            var dam_sensor = JSON.parse($(".data_damview").text());
            drawing(0, false);
            $("#simulate_start").click(function () {
                $("#time_step").text("Processing...");
                $(".sim_start_btn").fadeOut(200, function () {
                    $(".timeline_step").fadeIn(200, function () {
                        MainControll(dam_sensor);
                    });
                });
            });
        });
    </script>
<?php else: ?>
    <div class="row" style="margin-bottom: 30px;">
        <h3 class="text-center">ไม่สามารถแสดงผลข้อมูลและจำลองสถาณการณ์</h3>
        <p class="text-center">เนื่องจากไม่พบข้อมูลในช่วงเวลาที่ได้กำหนด</p>
    </div>
<?php endif; ?>

<script type="text/javascript">
    $(function () {
        $('#start_datetime24').combodate();
        $('#end_datetime24').combodate();

        $('#reset_btn').click(function () {
            location.href = location.origin + location.pathname;
        });
    });
</script>
@stop