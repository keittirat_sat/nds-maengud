@extends('layouts.template-20151218')
@section('content')
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<div class="row">
    <div class="col-md-4">
        <h1 class='gray quark_bold margin_bottom_0'>โครงการ<?php echo Config::get("{$profile}/nds-cloud.site_name"); ?></h1>
        <p class="gray">ระบบรายงานการสั่นสะเทือนภายในเขื่อน</p>
    </div>
    <div class="col-md-8 txt_right">
        <div class="col-md-12 txt_right">
            <div style="margin-top: 12px;">
                <h1 class='gray quark_bold margin_bottom_0'><b><?php echo Config::get("{$profile}/nds-cloud.seismic.station.{$station}"); ?></b></h1>
                <form class='margin_bottom_0' action="<?php echo action("HomeController@getSeismic"); ?>" id="filter_by_date">
                    <p class='margin_bottom_0'>
                        <label class="hidden-sm hidden-xs" for="forStartDate" style="margin-top: 0; margin-bottom: 0; padding: 7px 5px 0px 0px;">ข้อมูลหลังจากเวลา</label>
                        <input type="text" name="start_date" id="start_datetime24" data-format="YYYY-MM-DD HH:mm:ss" data-template="DD / MM / YYYY     HH : mm" name="datetime" value="<?php echo $current ?>">
                    </p>
                    <p class='txt_right margin_bottom_0'>
                        <i class="fa fa-refresh fa-spin" id="loading_spin" style="opacity: 0;"></i>&nbsp;
                        <select name="station" class="form-control" style="width: 250px; display: inline-block;" id="station_lock_time">
                            <?php foreach (Config::get("{$profile}/nds-cloud.seismic.station") as $each_station => $each_label): ?>
                                <option value="<?php echo $each_station; ?>" <?php echo strtolower($each_station) == strtolower($station) ? "selected" : ""; ?>><?php echo $each_label; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <button type='submit' class='btn btn-danger' id="submit_btn">ดำเนินการ</button>
                        <a href="<?php echo action("HomeController@getSeismic", array("station" => $station)); ?>" class='btn btn-warning'>กลับสู่เวลาปัจจุบัน</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if ($error == 0): ?>

    <div class='row margin_bottom_20'>
        <div class='col-md-12'>
            <div id='display_chart_ch_max' style="width: 100%; min-height: 400px; margin: 20px auto 0;"></div>
            <div id='display_chart_ch0' style="width: 100%; min-height: 400px; margin: 0px auto;"></div>
            <div id='display_chart_ch1' style="width: 100%; min-height: 400px; margin: 0px auto;"></div>
            <div id='display_chart_ch2' style="width: 100%; min-height: 400px; margin: 0px auto;"></div>
        </div>
    </div>

    <div class="hidden" id="seismic_all_data"><?php echo json_encode($raws_data); ?></div>

    <script type="text/javascript">
        var station = "<?php echo $station; ?>";

        function drawChartMax(ch0, ch1, ch2) {
            Highcharts.setOptions({
                global: {
                    useUTC: false,
                    timezoneOffset: 60 * 7
                }
            });

            $('#display_chart_ch_max').highcharts({
                chart: {
                    zoomType: 'x'
                },
                title: {
                    text: 'Realtime Acceleration',
                    x: -20 //center
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                },
                tooltip: {
                    valueSuffix: 'm/s^2'
                },
    //                legend: {
    //                    layout: 'vertical',
    //                    align: 'right',
    //                    verticalAlign: 'middle',
    //                    borderWidth: 0
    //                },
                series: [{
                        name: 'Ch0',
                        data: ch0
                    }, {
                        name: 'Ch1',
                        data: ch1
                    }, {
                        name: 'Ch2',
                        data: ch2
                    }]
            });
        }

        function drawChartCh(container, ch_number, ranges, channel) {

            Highcharts.setOptions({
                global: {
                    useUTC: false,
                    timezoneOffset: 60 * 7
                }
            });

            $('#' + container).highcharts({
                title: {
                    text: 'DAS ' + station.toUpperCase() + ' Ch: ' + ch_number + ' [' + channel + ']'
                },
                chart: {
                    zoomType: 'x'
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                tooltip: {
                    crosshairs: true,
                    shared: true,
                    valueSuffix: 'm/s^2'
                },
                legend: {
                },
                series: [
                    {
                        name: 'Range',
                        data: ranges,
                        turboThreshold: 0,
                        type: 'arearange',
                        lineWidth: 0,
                        linkedTo: ':previous',
                        color: Highcharts.getOptions().colors[0],
                        fillOpacity: 0.95,
                        zIndex: 0
                    }
                ]
            });
        }

    </script>
<?php else: ?>
    <div class='row margin_bottom_20' style='margin-top: 30px;'>
        <div class="col-xs-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <i class="glyphicon glyphicon-remove-circle"></i>&nbsp;ไม่สามารถแสดงผลกราฟข้อมูล
                </div>
                <div class="panel-body">
                    <h1 style='text-align: center;'>ข้อมูลไม่เพียงพอในช่วงระยะเวลาที่กำหนด กรุณาลองใหม่อีกครั้ง</h1>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if(isset($status['station_info'])): ?>
    <ul class="seismic_status">
        <?php $current_time = time(); ?>
        <?php foreach ($status['station_info'] as $each_status_station): ?>
            <li>
                <?php $sync_time = strtotime($each_status_station['save_at']['date']); ?>
                <?php $is_online = ($current_time - $sync_time) < 900 ? true : false; ?>
                <span  data-toggle="tooltip" data-placement="top" title="<?php echo "Last sync: " . date("Y-m-d H:i:s", $sync_time); ?>" class="label <?php echo $is_online ? "label-success" : "label-danger"; ?>"><?php echo $each_status_station['station']; ?></span>&nbsp;<span class="sync_time"><?php echo $each_status_station['sync_at']; ?></span>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<style type="text/css">
    ul.seismic_status{
        padding: 5px 10px;
        list-style: none;
    }

    ul.seismic_status li{
        display: inline-block;
        padding: 0px 5px;
    }

    ul.seismic_status li span.sync_time{
        font-size: 12px;
    }

    ul.seismic_status li span[data-toggle=tooltip]{        
        cursor: pointer;
    }
</style>

<script type="text/javascript">
    var ranges_0 = [];
    var ranges_1 = [];
    var ranges_2 = [];

    var ranges_0_max = [];
    var ranges_1_max = [];
    var ranges_2_max = [];

    var raws_data = $('#seismic_all_data').text();
    raws_data = $.trim(raws_data);
    if (raws_data) {
        raws_data = JSON.parse(raws_data);
        $.each(raws_data, function (i, v) {
            var unixdate = parseInt(i);
            var millisec = unixdate * 1000;

            ranges_0.push([millisec, v.ch0[0], v.ch0[1]]);
            ranges_1.push([millisec, v.ch1[0], v.ch1[1]]);
            ranges_2.push([millisec, v.ch2[0], v.ch2[1]]);

            ranges_0_max.push([millisec, v.ch0_max]);
            ranges_1_max.push([millisec, v.ch1_max]);
            ranges_2_max.push([millisec, v.ch2_max]);
        });
    }

    $(window).on('load', function () {
        $('#start_datetime24').combodate();

        $('#filter_by_date').submit(function () {
            $('#loading_spin').animate({"opacity": 1}, 200);
            return true;
        });

        $('#station_lock_time').change(function () {
            $('#filter_by_date').submit();
        });

        $('[data-toggle="tooltip"]').tooltip();

        if (raws_data) {
            drawChartMax(ranges_0_max, ranges_1_max, ranges_2_max);
            drawChartCh('display_chart_ch0', 0, ranges_0, 'Z');
            drawChartCh('display_chart_ch1', 1, ranges_1, 'N');
            drawChartCh('display_chart_ch2', 2, ranges_2, 'E');
        }

    });
</script>
@stop
