@extends('layouts.template-20151218')
@section('content')
<div class="row">
    <div class="col-md-12">
        <h1 class='gray quark_bold margin_bottom_0'>โครงการ<?php echo Config::get("{$profile}/nds-cloud.site_name")." ".$text; ?></h1>
        <p class="gray">ระบบรายงานสภาพแรงดันภายในเขื่อน</p>
    </div>
</div>

<div class="row">
    <canvas id="damCanvas" class="damview" width="1200" height="500"></canvas>
</div>

<div class="hidden dam_template"><?php echo $damview; ?></div>
<div class='hidden dam_data'><?php echo $data ?></div>

<script type="text/javascript">
    $(function () {
        var sensors = $.parseJSON($('.dam_data').text());
//        console.log(sensors);
        $('canvas.damview').drawImage({
            source: $(".dam_template").text(),
            width: 1200,
            height: 500,
            x: 600,
            y: 250
        });

        $.each(sensors[0].data, function (i, v) {
            $('canvas.damview').drawArc({
                fillStyle: "black",
                x: v.coor[0], y: v.coor[1],
                radius: 2
            });
            $('canvas.damview').drawText({
                fillStyle: '#25a',
                strokeWidth: 1,
                rotate: -25,
                x: v.coor[0] - 23, y: v.coor[1] + 10,
                fontSize: 10,
                fontFamily: 'Verdana, sans-serif',
                text: v.value.toFixed(4)
            });
        });
    });
</script>
@stop