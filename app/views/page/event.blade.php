@extends('layouts.template-20151218')
@section('content')
<h1 class="quark_bold">ประมวลเหตุการณ์แรงสั่นสะเทือนที่ผ่านมา</h1>

<div class="well well-sm">
    <h4 style='margin: 5px 0px 10px;'>รายละเอียด</h4>
    ระบบจัดเก็บค่าแรงสั่นสะเทือนที่มีความเร่งในหน่วย G โดยระบบจะเริ่มเก็บข้อมูลตั้งแต่ <span class="label label-warning"><?php echo $logging_info['log']; ?>G.</span> และจะมีการแจ้งเตือนเมื่อค่า G. มากกว่า <span class="label label-danger"><?php echo $logging_info['alarm']; ?>G.</span>  
</div>


<ul style="list-style: none;">
    <li>
        <strong></strong>
    </li>
    <li>
        <strong></strong>
    </li>
</ul>


<table class="table table-cover">
    <thead>
        <tr>
            <th>วันที่</th>
            <th>เวลา</th>
            <th>ค่า G สูงสุด</th>
            <th>การแจ้งเตือน</th>
            <?php foreach (Config::get("{$profile}/nds-cloud.seismic.station") as $each_station => $each_label): ?>
                <th style="text-align: center;"><?php echo strtoupper($each_station); ?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php if (count($data)): ?>
            <?php foreach ($data as $each_event): ?>
                <tr>
                    <?php $date = explode(' ', $each_event["timestamp"]); ?>
                    <td><?php echo $date[0]; ?></td>
                    <td><?php echo $date[1]; ?></td>
                    <td>
                        <span class="show_g" data-toggle="tooltip" data-placement="top" title="<?php echo strtolower($each_event["g"]); ?>"><?php echo number_format($each_event["g"], 7); ?>G.</span>
                    </td>
                    <td><?php echo $each_event["alert"] == true ? "<span class='label label-danger'>ALERT</span>" : "<span class='label label-warning'>NONE</span>"; ?></td>
                    <?php foreach (Config::get("{$profile}/nds-cloud.seismic.station") as $each_station => $each_label): ?>
                        <td>
                            <a class="btn btn-default btn-block" target="_blank" href="<?php echo action("HomeController@getSeismic", ["station" => $each_station, "time" => $each_event["timestamp"]]); ?>"><?php echo strtoupper($each_station); ?></a>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="<?php echo count(Config::get("nds-cloud.seismic.station")) + 4; ?>">
                    <h2 class="text-center" style="margin: 30px 0px;">ไม่พบข้อมูลเหตุการ์ณแผ่นดินไหว</h2>
                </td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>

<div class="" style="padding: 0 0 25px;">
    <?php
    $pagination = new ZebraPagination();
    $pagination->records($info['total_data']);
    $pagination->records_per_page($records_per_page);
    $pagination->padding(false);
    $pagination->render();
    ?>
</div>

<ul class="seismic_status">
    <?php $current_time = time(); ?>
    <?php foreach ($status['station_info'] as $each_status_station): ?>
        <li>
            <?php $sync_time = strtotime($each_status_station['save_at']['date']); ?>
            <?php $is_online = ($current_time - $sync_time) < 900 ? true : false; ?>
            <span  data-toggle="tooltip" data-placement="top" title="<?php echo "Last sync: " . date("Y-m-d H:i:s", $sync_time); ?>" class="label <?php echo $is_online ? "label-success" : "label-danger"; ?>"><?php echo $each_status_station['station']; ?></span>&nbsp;<span class="sync_time"><?php echo $each_status_station['sync_at']; ?></span>
        </li>
    <?php endforeach; ?>
</ul>

<style type="text/css">
    ul.seismic_status{
        padding: 5px 10px;
        list-style: none;
    }

    ul.seismic_status li{
        display: inline-block;
        padding: 0px 5px;
    }

    ul.seismic_status li span.sync_time{
        font-size: 12px;
    }

    ul.seismic_status li span[data-toggle=tooltip]{        
        cursor: pointer;
    }
</style>

<style type="text/css">
    .show_g{
        cursor: pointer;
        border-bottom: 1px dotted #000;
        text-decoration: none;
    }
</style>

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop