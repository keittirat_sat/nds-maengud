<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FakeCoordinate
 *
 * @author keitt
 */
class FakeCoordinate
{

    private static $_instance;

    public static function instance()
    {
        self::$_instance = empty(self::$_instance) ? new FakeCoordinate() : self::$_instance;
        return self::$_instance;
    }

    public function maengud($data_list)
    {
        for ($j = 51; $j <= 65; $j++) {
            if ($j <= 55) {
                $temp_val = $data_list["p1"]['value'];
                $temp_coor = $data_list["p1"]['coor'];
                $step_dif = 50;
            } elseif ($j <= 59) {
                $temp_val = $data_list["p2"]['value'];
                $temp_coor = $data_list["p2"]['coor'];
                $step_dif = 55;
            } elseif ($j <= 63) {
                $temp_val = $data_list["p5"]['value'];
                $temp_coor = $data_list["p5"]['coor'];
                $step_dif = 59;
            } elseif ($j <= 65) {
                $temp_val = $data_list["p9"]['value'];
                $temp_coor = $data_list["p9"]['coor'];
                $step_dif = 63;
            }

            $step_change_x = abs($j - $step_dif) * 58;
            $temp_coor[0] = $temp_coor[0] - $step_change_x;
            $data_list["p{$j}"]["coor"] = $temp_coor;
            $data_list["p{$j}"]["value"] = $temp_val;
            $data_list["p{$j}"]["extend"] = true;
        }

        return $data_list;
    }

    public function huaimanao($data_list)
    {
        /**
         * Extend - Y Exist
         */
        $data_list['p13'] = [
            'coor' => [$data_list['p4']['coor'][0], (($data_list['p4']['coor'][1] + $data_list['p3']['coor'][1]) / 2)],
            'value' => ($data_list['p4']['value'] + $data_list['p3']['value']) / 2,
            'extend' => 1
        ];
        
        $data_list['p14'] = [
            'coor' => [$data_list['p3']['coor'][0], (($data_list['p3']['coor'][1] + $data_list['p2']['coor'][1]) / 2)],
            'value' => ($data_list['p3']['value'] + $data_list['p2']['value']) / 2,
            'extend' => 1
        ];
        
        $data_list['p15'] = [
            'coor' => [$data_list['p8']['coor'][0], (($data_list['p8']['coor'][1] + $data_list['p7']['coor'][1]) / 2)],
            'value' => ($data_list['p8']['value'] + $data_list['p7']['value']) / 2,
            'extend' => 1
        ];
        
        $data_list['p16'] = [
            'coor' => [$data_list['p7']['coor'][0], (($data_list['p7']['coor'][1] + $data_list['p6']['coor'][1]) / 2)],
            'value' => ($data_list['p7']['value'] + $data_list['p6']['value']) / 2,
            'extend' => 1
        ];
        
        /**
         * Divide: 1
         */
        $data_list['p17'] = [
            'coor' => [(($data_list['p4']['coor'][0] + $data_list['p8']['coor'][0]) / 2), $data_list['p4']['coor'][1]],
            'value' => ($data_list['p4']['value'] + $data_list['p8']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p18'] = [
            'coor' => [(($data_list['p13']['coor'][0] + $data_list['p15']['coor'][0]) / 2), $data_list['p13']['coor'][1]],
            'value' => ($data_list['p13']['value'] + $data_list['p15']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p19'] = [
            'coor' => [(($data_list['p3']['coor'][0] + $data_list['p7']['coor'][0]) / 2), $data_list['p3']['coor'][1]],
            'value' => ($data_list['p3']['value'] + $data_list['p7']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p20'] = [
            'coor' => [(($data_list['p14']['coor'][0] + $data_list['p16']['coor'][0]) / 2), $data_list['p14']['coor'][1]],
            'value' => ($data_list['p14']['value'] + $data_list['p16']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p21'] = [
            'coor' => [(($data_list['p2']['coor'][0] + $data_list['p6']['coor'][0]) / 2), $data_list['p2']['coor'][1]],
            'value' => ($data_list['p2']['value'] + $data_list['p6']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p22'] = [
            'coor' => [(($data_list['p1']['coor'][0] + $data_list['p5']['coor'][0]) / 2), $data_list['p1']['coor'][1]],
            'value' => ($data_list['p1']['value'] + $data_list['p5']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Divide: 2
         */
        $data_list['p23'] = [
            'coor' => [(($data_list['p4']['coor'][0] + $data_list['p17']['coor'][0]) / 2), $data_list['p4']['coor'][1]],
            'value' => ($data_list['p4']['value'] + $data_list['p17']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p24'] = [
            'coor' => [(($data_list['p13']['coor'][0] + $data_list['p18']['coor'][0]) / 2), $data_list['p13']['coor'][1]],
            'value' => ($data_list['p13']['value'] + $data_list['p18']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p25'] = [
            'coor' => [(($data_list['p3']['coor'][0] + $data_list['p19']['coor'][0]) / 2), $data_list['p3']['coor'][1]],
            'value' => ($data_list['p3']['value'] + $data_list['p19']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p26'] = [
            'coor' => [(($data_list['p2']['coor'][0] + $data_list['p20']['coor'][0]) / 2), $data_list['p2']['coor'][1]],
            'value' => ($data_list['p2']['value'] + $data_list['p20']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p27'] = [
            'coor' => [(($data_list['p14']['coor'][0] + $data_list['p21']['coor'][0]) / 2), $data_list['p14']['coor'][1]],
            'value' => ($data_list['p14']['value'] + $data_list['p21']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p28'] = [
            'coor' => [(($data_list['p1']['coor'][0] + $data_list['p22']['coor'][0]) / 2), $data_list['p1']['coor'][1]],
            'value' => ($data_list['p1']['value'] + $data_list['p22']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Divide: 3
         */
        $data_list['p29'] = [
            'coor' => [(($data_list['p17']['coor'][0] + $data_list['p8']['coor'][0]) / 2), $data_list['p17']['coor'][1]],
            'value' => ($data_list['p17']['value'] + $data_list['p8']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p30'] = [
            'coor' => [(($data_list['p18']['coor'][0] + $data_list['p15']['coor'][0]) / 2), $data_list['p18']['coor'][1]],
            'value' => ($data_list['p18']['value'] + $data_list['p15']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p31'] = [
            'coor' => [(($data_list['p19']['coor'][0] + $data_list['p7']['coor'][0]) / 2), $data_list['p19']['coor'][1]],
            'value' => ($data_list['p19']['value'] + $data_list['p7']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p32'] = [
            'coor' => [(($data_list['p20']['coor'][0] + $data_list['p16']['coor'][0]) / 2), $data_list['p20']['coor'][1]],
            'value' => ($data_list['p20']['value'] + $data_list['p16']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p33'] = [
            'coor' => [(($data_list['p21']['coor'][0] + $data_list['p6']['coor'][0]) / 2), $data_list['p21']['coor'][1]],
            'value' => ($data_list['p21']['value'] + $data_list['p6']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p34'] = [
            'coor' => [(($data_list['p22']['coor'][0] + $data_list['p5']['coor'][0]) / 2), $data_list['p22']['coor'][1]],
            'value' => ($data_list['p22']['value'] + $data_list['p5']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Divide: 4 - Extend to left core zone
         */
       $data_list['p35'] = [
           'coor' => [($data_list['p4']['coor'][0] - ($data_list['p23']['coor'][0] - $data_list['p4']['coor'][0])), $data_list['p4']['coor'][1]],
           'value' => ($data_list['p4']['value'] - $data_list['p23']['value']) + $data_list['p4']['value'],
           'extend' => 1
       ];

       $data_list['p36'] = [
           'coor' => [($data_list['p13']['coor'][0] - ($data_list['p24']['coor'][0] - $data_list['p13']['coor'][0])), $data_list['p13']['coor'][1]],
           'value' => ($data_list['p13']['value'] - $data_list['p24']['value']) + $data_list['p13']['value'],
           'extend' => 1
       ];

       $data_list['p37'] = [
           'coor' => [($data_list['p3']['coor'][0] - ($data_list['p25']['coor'][0] - $data_list['p3']['coor'][0])), $data_list['p3']['coor'][1]],
           'value' => ($data_list['p3']['value'] - $data_list['p25']['value']) + $data_list['p3']['value'],
           'extend' => 1
       ];

       $data_list['p38'] = [
           'coor' => [($data_list['p14']['coor'][0] - ($data_list['p26']['coor'][0] - $data_list['p14']['coor'][0])), $data_list['p14']['coor'][1]],
           'value' => ($data_list['p14']['value'] - $data_list['p26']['value']) + $data_list['p14']['value'],
           'extend' => 1
       ];

       $data_list['p39'] = [
           'coor' => [($data_list['p2']['coor'][0] - ($data_list['p27']['coor'][0] - $data_list['p2']['coor'][0])), $data_list['p2']['coor'][1]],
           'value' => ($data_list['p2']['value'] - $data_list['p27']['value']) + $data_list['p2']['value'],
           'extend' => 1
       ];

       $data_list['p40'] = [
           'coor' => [
                ($data_list['p1']['coor'][0] - ($data_list['p28']['coor'][0] - $data_list['p1']['coor'][0])), 
                $data_list['p1']['coor'][1]
            ],
           'value' => ($data_list['p1']['value'] - $data_list['p28']['value']) + $data_list['p1']['value'],
           'extend' => 1
       ];


        return $data_list;
    }

    public function huaikaew($data_list)
    {
        /**
         * Section 1
         */
        $data_list['p21'] = [
            'coor' => [
                $data_list['p1']['coor'][0],
                ($data_list['p1']['coor'][1] + $data_list['p2']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p2']['value'] + $data_list['p1']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p22'] = [
            'coor' => [
                $data_list['p1']['coor'][0],
                ($data_list['p2']['coor'][1] + $data_list['p3']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p3']['value'] + $data_list['p2']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 2
         */
        $data_list['p23'] = [
            'coor' => [
                $data_list['p4']['coor'][0],
                ($data_list['p4']['coor'][1] + $data_list['p5']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p5']['value'] + $data_list['p4']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p24'] = [
            'coor' => [
                $data_list['p4']['coor'][0],
                ($data_list['p5']['coor'][1] + $data_list['p6']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p6']['value'] + $data_list['p5']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p25'] = [
            'coor' => [
                $data_list['p4']['coor'][0],
                ($data_list['p6']['coor'][1] + $data_list['p7']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p7']['value'] + $data_list['p6']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 3
         */
        $data_list['p26'] = [
            'coor' => [
                $data_list['p8']['coor'][0],
                ($data_list['p8']['coor'][1] + $data_list['p9']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p9']['value'] + $data_list['p8']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p27'] = [
            'coor' => [
                $data_list['p8']['coor'][0],
                ($data_list['p9']['coor'][1] + $data_list['p10']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p10']['value'] + $data_list['p9']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p28'] = [
            'coor' => [
                $data_list['p8']['coor'][0],
                ($data_list['p10']['coor'][1] + $data_list['p11']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p11']['value'] + $data_list['p10']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 4
         */
        $data_list['p29'] = [
            'coor' => [
                $data_list['p12']['coor'][0],
                ($data_list['p12']['coor'][1] + $data_list['p13']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p13']['value'] + $data_list['p12']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p30'] = [
            'coor' => [
                $data_list['p12']['coor'][0],
                ($data_list['p13']['coor'][1] + $data_list['p14']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p14']['value'] + $data_list['p13']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p31'] = [
            'coor' => [
                $data_list['p12']['coor'][0],
                ($data_list['p14']['coor'][1] + $data_list['p15']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p15']['value'] + $data_list['p14']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 5
         */

        $data_list['p32'] = [
            'coor' => [
                $data_list['p16']['coor'][0],
                ($data_list['p16']['coor'][1] + $data_list['p17']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p17']['value'] + $data_list['p16']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p33'] = [
            'coor' => [
                $data_list['p16']['coor'][0],
                ($data_list['p17']['coor'][1] + $data_list['p18']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p18']['value'] + $data_list['p17']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 6
         */

        $data_list['p34'] = [
            'coor' => [
                $data_list['p19']['coor'][0],
                ($data_list['p19']['coor'][1] + $data_list['p20']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p20']['value'] + $data_list['p19']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 7 - New Rows
         */
        $data_list['p35'] = [
            'coor' => [
                ($data_list['p1']['coor'][0] + $data_list['p4']['coor'][0]) / 2,
                ($data_list['p1']['coor'][1] + $data_list['p4']['coor'][1]) / 2,
            ],
            'value'  => ($data_list['p1']['value'] + $data_list['p4']['value']) / 2,
            'extend' => 1
        ];
                
        $data_list['p36'] = [
            'coor' => [
                ($data_list['p2']['coor'][0] + $data_list['p5']['coor'][0]) / 2,
                ($data_list['p2']['coor'][1] + $data_list['p5']['coor'][1]) / 2,
            ],
            'value'  => ($data_list['p2']['value'] + $data_list['p5']['value']) / 2,
            'extend' => 1
        ];
               
       $data_list['p37'] = [
            'coor' => [
                ($data_list['p3']['coor'][0] + $data_list['p6']['coor'][0]) / 2,
                ($data_list['p3']['coor'][1] + $data_list['p6']['coor'][1]) / 2,
            ],
            'value'  => ($data_list['p3']['value'] + $data_list['p6']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 8 - New Rows
         */
               
       $data_list['p38'] = [
            'coor' => [
                ($data_list['p4']['coor'][0] + $data_list['p8']['coor'][0]) / 2,
                ($data_list['p4']['coor'][1] + $data_list['p8']['coor'][1]) / 2,
            ],
            'value'  => ($data_list['p4']['value'] + $data_list['p8']['value']) / 2,
            'extend' => 1
        ];
               
       $data_list['p39'] = [
            'coor' => [
                ($data_list['p5']['coor'][0] + $data_list['p9']['coor'][0]) / 2,
                ($data_list['p5']['coor'][1] + $data_list['p9']['coor'][1]) / 2,
            ],
            'value'  => ($data_list['p5']['value'] + $data_list['p9']['value']) / 2,
            'extend' => 1
        ];
               
       $data_list['p40'] = [
            'coor' => [
                ($data_list['p6']['coor'][0] + $data_list['p10']['coor'][0]) / 2,
                ($data_list['p6']['coor'][1] + $data_list['p10']['coor'][1]) / 2,
            ],
            'value'  => ($data_list['p6']['value'] + $data_list['p10']['value']) / 2,
            'extend' => 1
        ];
               
       $data_list['p41'] = [
            'coor' => [
                ($data_list['p7']['coor'][0] + $data_list['p11']['coor'][0]) / 2,
                ($data_list['p7']['coor'][1] + $data_list['p11']['coor'][1]) / 2,
            ],
            'value'  => ($data_list['p7']['value'] + $data_list['p11']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 9 - New Rows
         */
        
        $data_list['p42'] = [
            'coor' => [
                ($data_list['p8']['coor'][0] + $data_list['p12']['coor'][0]) / 2,
                ($data_list['p8']['coor'][1] + $data_list['p12']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p8']['value'] + $data_list['p12']['value']) / 2,
            'extend' => 1
        ];
        
        $data_list['p43'] = [
            'coor' => [
                ($data_list['p9']['coor'][0] + $data_list['p13']['coor'][0]) / 2,
                ($data_list['p9']['coor'][1] + $data_list['p13']['coor'][1]) / 2,
            ],
            'value'  => ($data_list['p9']['value'] + $data_list['p13']['value']) / 2,
            'extend' => 1
        ];
        
        $data_list['p44'] = [
            'coor' => [
                ($data_list['p10']['coor'][0] + $data_list['p14']['coor'][0]) / 2,
                ($data_list['p10']['coor'][1] + $data_list['p14']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p10']['value'] + $data_list['p14']['value']) / 2,
            'extend' => 1
        ];
        
        $data_list['p45'] = [
            'coor' => [
                ($data_list['p11']['coor'][0] + $data_list['p15']['coor'][0]) / 2,
                ($data_list['p11']['coor'][1] + $data_list['p15']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p11']['value'] + $data_list['p15']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 10 - New Rows
         */
        $data_list['p46'] = [
            'coor' => [
                ($data_list['p12']['coor'][0] + $data_list['p16']['coor'][0]) / 2,
                ($data_list['p12']['coor'][1] + $data_list['p16']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p12']['value'] + $data_list['p16']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p47'] = [
            'coor' => [
                ($data_list['p13']['coor'][0] + $data_list['p17']['coor'][0]) / 2,
                ($data_list['p13']['coor'][1] + $data_list['p17']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p13']['value'] + $data_list['p17']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p48'] = [
            'coor' => [
                ($data_list['p14']['coor'][0] + $data_list['p18']['coor'][0]) / 2,
                ($data_list['p14']['coor'][1] + $data_list['p18']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p14']['value'] + $data_list['p18']['value']) / 2,
            'extend' => 1
        ];

        return $data_list;
    }

    public function maemoei($data_list)
    {
        /**
         * Section 1
         */
        $data_list['p11'] = [
            'coor' => [
                ($data_list['p1']['coor'][0] + $data_list['p2']['coor'][0]) / 2,
                ($data_list['p1']['coor'][1] + $data_list['p2']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p1']['value'] + $data_list['p2']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p12'] = [
            'coor' => [
                ($data_list['p2']['coor'][0] + $data_list['p3']['coor'][0]) / 2,
                ($data_list['p2']['coor'][1] + $data_list['p3']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p2']['value'] + $data_list['p3']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 2 - New Rows
         */

        $data_list['p13'] = [
            'coor' => [
                ($data_list['p1']['coor'][0] + $data_list['p4']['coor'][0]) / 2,
                ($data_list['p1']['coor'][1] + $data_list['p4']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p1']['value'] + $data_list['p4']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p14'] = [
            'coor' => [
                ($data_list['p2']['coor'][0] + $data_list['p5']['coor'][0]) / 2,
                ($data_list['p2']['coor'][1] + $data_list['p5']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p2']['value'] + $data_list['p5']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p15'] = [
            'coor' => [
                ($data_list['p3']['coor'][0] + $data_list['p6']['coor'][0]) / 2,
                ($data_list['p3']['coor'][1] + $data_list['p6']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p3']['value'] + $data_list['p6']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 3
         */
        $data_list['p16'] = [
            'coor' => [
                ($data_list['p4']['coor'][0] + $data_list['p5']['coor'][0]) / 2,
                ($data_list['p4']['coor'][1] + $data_list['p5']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p4']['value'] + $data_list['p5']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p17'] = [
            'coor' => [
                ($data_list['p5']['coor'][0] + $data_list['p6']['coor'][0]) / 2,
                ($data_list['p5']['coor'][1] + $data_list['p6']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p5']['value'] + $data_list['p6']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 4 - New Rows
         */
        $data_list['p18'] = [
            'coor' => [
                ($data_list['p5']['coor'][0] + $data_list['p9']['coor'][0]) / 2,
                ($data_list['p5']['coor'][1] + $data_list['p9']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p5']['value'] + $data_list['p9']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p19'] = [
            'coor' => [
                ($data_list['p4']['coor'][0] + $data_list['p9']['coor'][0]) / 2,
                ($data_list['p4']['coor'][1] + $data_list['p9']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p4']['value'] + $data_list['p9']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 5 - New Rows
         */
        $data_list['p20'] = [
            'coor' => [
                ($data_list['p4']['coor'][0] + $data_list['p18']['coor'][0]) / 2,
                ($data_list['p4']['coor'][1] + $data_list['p18']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p4']['value'] + $data_list['p18']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p21'] = [
            'coor' => [
                ($data_list['p4']['coor'][0] + $data_list['p19']['coor'][0]) / 2,
                ($data_list['p4']['coor'][1] + $data_list['p19']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p4']['value'] + $data_list['p19']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p22'] = [
            'coor' => [
                ($data_list['p5']['coor'][0] + $data_list['p18']['coor'][0]) / 2,
                ($data_list['p5']['coor'][1] + $data_list['p18']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p5']['value'] + $data_list['p18']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p23'] = [
            'coor' => [
                ($data_list['p5']['coor'][0] + $data_list['p19']['coor'][0]) / 2,
                ($data_list['p5']['coor'][1] + $data_list['p19']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p5']['value'] + $data_list['p19']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p24'] = [
            'coor' => [
                ($data_list['p17']['coor'][0] + $data_list['p19']['coor'][0]) / 2,
                ($data_list['p17']['coor'][1] + $data_list['p19']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p17']['value'] + $data_list['p19']['value']) / 2,
            'extend' => 1
        ];

        $data_list['p25'] = [
            'coor' => [
                ($data_list['p17']['coor'][0] + $data_list['p18']['coor'][0]) / 2,
                ($data_list['p17']['coor'][1] + $data_list['p18']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p17']['value'] + $data_list['p18']['value']) / 2,
            'extend' => 1
        ];

        /**
         * Section 6 - New Rows
         */


        $data_list['p26'] = [
            'coor' => [
                ($data_list['p18']['coor'][0] + $data_list['p9']['coor'][0]) / 2,
                ($data_list['p18']['coor'][1] + $data_list['p9']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p18']['value'] + $data_list['p9']['value']) / 2,
            'extend' => 1
        ];


        $data_list['p27'] = [
            'coor' => [
                ($data_list['p19']['coor'][0] + $data_list['p9']['coor'][0]) / 2,
                ($data_list['p19']['coor'][1] + $data_list['p9']['coor'][1]) / 2
            ],
            'value'  => ($data_list['p19']['value'] + $data_list['p9']['value']) / 2,
            'extend' => 1
        ];

        return $data_list;
    }
}
