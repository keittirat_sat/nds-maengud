<?php

class AssetMinify
{

    private static $_instance;

    public static function instance()
    {
        self::$_instance = empty(self::$_instance) ? new AssetMinify() : self::$_instance;
        return self::$_instance;
    }

    public function fixMinify($code)
    {
        $code = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $code);
        $code = preg_replace('/\/\/(.*)*\n/', '', $code);
        $code = str_replace(': ', ':', $code);
        return str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $code);
    }

    public function getHash()
    {
        return json_decode(file_get_contents(public_path('theme-version.json')), true);
    }

    public function build()
    {
        $original_hash = json_decode(file_get_contents(public_path('theme-version.json')), true);
        $base_original = 'minify-asset.list';
        $hash_version = 'minify-asset.hash';

        $asset_list = Config::get($base_original);
        $new_list = array();
        foreach ($asset_list as $type_file => $file_list) {
            foreach ($file_list as $each_key_file => $each_old_hash) {
                $temp_key = $hash_version . '.' . $type_file . '.' . $each_key_file;
                $temp_value = md5_file(public_path() . Config::get($base_original . '.' . $type_file . '.' . $each_key_file));
                $new_list[$temp_key] = $temp_value;
            }
        }

        $is_diff = array_diff($new_list, $original_hash);
        if (empty($is_diff)) {
            return false;
        }

        /**
         * CSS Compression
         */
        $buffer = [];
        foreach ($asset_list['css'] as $key => $path) {
            $temp = file_get_contents(public_path() . $path);
            $buffer[] = self::fixMinify($temp);
        }
        $str_line = '//NDS CSS Generator: ' . date('j F Y H:i:s') . PHP_EOL;
        $str_line .= implode('', $buffer);
        file_put_contents(public_path('css/app.min.css'), $str_line);

        /**
         * JS Compression
         */
        $buffer = [];
        foreach ($asset_list['js'] as $key => $path) {
            $temp = file_get_contents(public_path() . $path);
            $buffer[] = self::fixMinify($temp);
        }

        $str_line = '//NDS Javascript Generator: ' . date('j F Y H:i:s') . PHP_EOL;
        $str_line .= implode('', $buffer);
        file_put_contents(public_path('js/app.min.js'), $str_line);
        $new_list['app-js'] = md5_file(public_path('js/app.min.js'));
        $new_list['app-css'] = md5_file(public_path('css/app.min.css'));
        file_put_contents(public_path('theme-version.json'), json_encode($new_list));
        return true;
    }
}
