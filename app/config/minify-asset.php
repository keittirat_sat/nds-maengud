<?php
return array(
    'list' => array(
        'css' => array(
            'bootstrap-css' => '/css/bootstrap.min.css',
            'font-quark-css' => '/fonts/Quark/quark-mod.css',
            'maengud-css' => '/css/maengud.css',
            'zebra-pagination-css' => '/css/zebra_pagination.css',
            'font-awesome-css' => '/css/font-awesome.min.css'
        ),
        'js' => array(
//            'jquery-js' => '/js/jquery-1.7.min.js',
            'bootstrap-js' => '/js/bootstrap.min.js',
            'math-js' => '/js/math.min.js',
            'tween-max-js' => '/js/TweenMax.min.js',
            'jquery-gsap-js' => '/js/jquery.gsap.min.js',
//            'moment-js' => '/js/moment.min.js',
            'combodate-js' => '/js/combodate.js',
            'jcanvas-js' => '/js/jcanvas.min.js',
            'simulation-js' => '/js/beanfield.js',
            'zebra-pagination-js' => '/js/zebra_pagination.js'
        )
    )
);

