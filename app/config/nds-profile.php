<?php

return array(
	"maengud"	 => array('maengud.damlog.com', 'maengud.damlog.dev', 'alpha-maengud.damlog.com'),
	"maetalop"	 => array('maetalop.damlog.com', 'maetalop.damlog.dev', 'alpha-maetalop.damlog.com'),
	"huaimanao"	 => array('huaimanao.damlog.com', 'huaimanao.damlog.dev', 'alpha-huaimanao.damlog.com'),
	"huaikaew"	 => array('huaikaew.damlog.com', 'huaikaew.damlog.dev', 'alpha-huaikaew.damlog.com'),
	"maehongson" => array('maehongson.damlog.com', 'maehongson.damlog.dev', 'alpha-maehongson.damlog.com'),
	"maemoei"	 => array('maemoei.damlog.com', 'maemoei.damlog.dev', 'alpha-maemoei.damlog.com')
);
